from django.shortcuts import render, redirect
from crud.forms import StudentForm
from crud.models import Student

# Create your views here.
def create_student(request):
	if request.method =="POST":
		form = StudentForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/display-student')
	else:
		form = StudentForm()
	return render(request, 'create-student.html',{'form':form})
def display_student(request):
	students = Student.objects.all()
	return render(request, 'display-student.html',{'students':students})
def delete(request, id):
	student = Student.objects.get(id=id)
	student.delete()
	return redirect('/display-student')
def edit_student(request, id):
	student = Student.objects.get(id=id)
	return render(request, 'edit-student.html', {'student':student})
def update_student(request, id):
	student = Student.objects.get(id=id)
	form = StudentForm(request.POST, instance = student)
	if form.is_valid():
		form.save()
		return redirect('/display-student')
	return render(request, 'edit-student.html',{'student':student})